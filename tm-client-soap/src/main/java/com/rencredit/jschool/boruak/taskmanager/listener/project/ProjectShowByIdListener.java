package com.rencredit.jschool.boruak.taskmanager.listener.project;

import com.rencredit.jschool.boruak.taskmanager.endpoint.soap.*;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.event.ConsoleEvent;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyProjectException;
import com.rencredit.jschool.boruak.taskmanager.listener.AbstractListener;
import com.rencredit.jschool.boruak.taskmanager.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import static com.rencredit.jschool.boruak.taskmanager.util.TerminalUtil.showProject;

@Component
public class ProjectShowByIdListener extends AbstractListener {

    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-view-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Show project by id.";
    }

    @Override
    @EventListener(condition = "@projectShowByIdListener.name() == #event.command")
    public void handle(final ConsoleEvent event) throws EmptyProjectException, EmptyUserIdException_Exception, EmptyLoginException_Exception, EmptyIdException_Exception, UnknownUserException_Exception, DeniedAccessException_Exception {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();

        @Nullable final ProjectDTO project = projectEndpoint.findProjectByIdDTO(id);
        showProject(project);
        System.out.println("[OK]");
    }

}
