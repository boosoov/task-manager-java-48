package com.rencredit.jschool.boruak.taskmanager.exception.logic;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractClientException;

public class InternalProgramException extends AbstractClientException {

    public InternalProgramException() {
        super("Internal program logic exception...");
    }

}
