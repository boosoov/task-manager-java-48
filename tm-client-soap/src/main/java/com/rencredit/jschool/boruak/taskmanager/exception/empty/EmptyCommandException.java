package com.rencredit.jschool.boruak.taskmanager.exception.empty;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractClientException;

public class EmptyCommandException extends AbstractClientException {

    public EmptyCommandException() {
        super("Error! Command is empty...");
    }

}
