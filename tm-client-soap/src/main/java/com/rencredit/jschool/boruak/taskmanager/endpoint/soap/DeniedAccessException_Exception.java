
package com.rencredit.jschool.boruak.taskmanager.endpoint.soap;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 3.2.7
 * 2021-03-08T20:46:37.766+03:00
 * Generated source version: 3.2.7
 */

@WebFault(name = "DeniedAccessException", targetNamespace = "http://soap.endpoint.taskmanager.boruak.jschool.rencredit.com/")
public class DeniedAccessException_Exception extends Exception {

    private com.rencredit.jschool.boruak.taskmanager.endpoint.soap.DeniedAccessException deniedAccessException;

    public DeniedAccessException_Exception() {
        super();
    }

    public DeniedAccessException_Exception(String message) {
        super(message);
    }

    public DeniedAccessException_Exception(String message, java.lang.Throwable cause) {
        super(message, cause);
    }

    public DeniedAccessException_Exception(String message, com.rencredit.jschool.boruak.taskmanager.endpoint.soap.DeniedAccessException deniedAccessException) {
        super(message);
        this.deniedAccessException = deniedAccessException;
    }

    public DeniedAccessException_Exception(String message, com.rencredit.jschool.boruak.taskmanager.endpoint.soap.DeniedAccessException deniedAccessException, java.lang.Throwable cause) {
        super(message, cause);
        this.deniedAccessException = deniedAccessException;
    }

    public com.rencredit.jschool.boruak.taskmanager.endpoint.soap.DeniedAccessException getFaultInfo() {
        return this.deniedAccessException;
    }
}
