#!/usr/bin/env bash

echo  'SHUTDOWN CLIENT...';

if [ ! -f ./tm-client.pid ]; then
	echo "tm-client pid not found"
	exit 1;
fi

echo 'KILL PROCESS WITH PID '$(cat ./tm-client.pid);
kill -9 $(cat ./tm-client.pid)
rm ./tm-client.pid
echo 'OK';
