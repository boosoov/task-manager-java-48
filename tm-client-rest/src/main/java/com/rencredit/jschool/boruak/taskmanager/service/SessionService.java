package com.rencredit.jschool.boruak.taskmanager.service;

import org.springframework.stereotype.Service;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.net.CookieManager;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Service
public class SessionService {

    private static final CookieManager session = new CookieManager();

    public static CookieManager getSession() {
        return session;
    }

}
