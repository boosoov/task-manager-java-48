package com.rencredit.jschool.boruak.taskmanager.endpoint.server;

import com.rencredit.jschool.boruak.taskmanager.dto.ProjectDTO;
import com.rencredit.jschool.boruak.taskmanager.service.SessionService;
import feign.Feign;
import feign.okhttp.OkHttpClient;
import okhttp3.JavaNetCookieJar;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.support.SpringDecoder;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.cloud.openfeign.support.SpringMvcContract;
import org.springframework.http.MediaType;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;

import java.net.CookieManager;
import java.net.CookiePolicy;

@RequestMapping("/api/project")
public interface IProjectRestEndpoint {

    static IProjectRestEndpoint client(final String baseUrl) {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;

        final CookieManager cookieManager = SessionService.getSession();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);

        final okhttp3.OkHttpClient.Builder builder =
                new okhttp3.OkHttpClient().newBuilder();
        builder.cookieJar(new JavaNetCookieJar(cookieManager));

        return Feign.builder()
                .client(new OkHttpClient(builder.build()))
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(IProjectRestEndpoint.class, baseUrl);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    void create(@RequestBody ProjectDTO project);

    @Nullable
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    ProjectDTO findOneByIdDTO(@PathVariable("id") String id);

    @GetMapping(value = "/exist/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    boolean existsById(@PathVariable("id") String id);

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    void deleteOneById(@PathVariable("id") String id);


}
