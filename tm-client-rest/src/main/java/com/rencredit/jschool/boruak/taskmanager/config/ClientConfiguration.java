package com.rencredit.jschool.boruak.taskmanager.config;


import com.rencredit.jschool.boruak.taskmanager.endpoint.server.*;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.rencredit.jschool.boruak.taskmanager")
public class ClientConfiguration {

    @Bean
    public IAuthRestEndpoint authRestEndpoint() {
        return IAuthRestEndpoint.client("http://localhost:8080/server/");
    }

    @Bean
    public IProjectRestEndpoint projectRestEndpoint() {
        return IProjectRestEndpoint.client("http://localhost:8080/server/");
    }

    @Bean
    public IProjectsRestEndpoint projectsRestEndpoint() {
        return IProjectsRestEndpoint.client("http://localhost:8080/server/");
    }

    @Bean
    public ITaskRestEndpoint taskRestEndpoint() {
        return ITaskRestEndpoint.client("http://localhost:8080/server/");
    }

    @Bean
    public ITasksRestEndpoint tasksRestEndpoint() {
        return ITasksRestEndpoint.client("http://localhost:8080/server/");
    }

}
