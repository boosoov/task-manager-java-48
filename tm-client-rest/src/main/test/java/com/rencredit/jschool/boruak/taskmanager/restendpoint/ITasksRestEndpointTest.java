package com.rencredit.jschool.boruak.taskmanager.restendpoint;

import com.rencredit.jschool.boruak.taskmanager.config.ClientConfiguration;
import com.rencredit.jschool.boruak.taskmanager.dto.TaskDTO;
import com.rencredit.jschool.boruak.taskmanager.endpoint.server.IAuthRestEndpoint;
import com.rencredit.jschool.boruak.taskmanager.endpoint.server.ITasksRestEndpoint;
import com.rencredit.jschool.boruak.taskmanager.marker.IntegrationWithServerTestCategory;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ClientConfiguration.class)
@Category(IntegrationWithServerTestCategory.class)
public class ITasksRestEndpointTest {

    @Autowired
    ITasksRestEndpoint tasksRestEndpoint;

    @Autowired
    IAuthRestEndpoint authRestEndpoint;

    @Before
    public void init()  {
        authRestEndpoint.login("test", "test");
    }

    @After
    public void clearAll() {
        authRestEndpoint.logout();
    }

    @Test
    public void test() {
        tasksRestEndpoint.deleteAll();
        List<TaskDTO> emptyList = tasksRestEndpoint.getListDTO();
        Assert.assertTrue(emptyList.isEmpty());
        List<TaskDTO> listForSave = new ArrayList<>();
        listForSave.add(new TaskDTO());
        listForSave.add(new TaskDTO());
        listForSave.add(new TaskDTO());
        tasksRestEndpoint.saveAll(listForSave);
        Assert.assertEquals(3, tasksRestEndpoint.count());
        Assert.assertEquals(3, tasksRestEndpoint.getListDTO().size());
        tasksRestEndpoint.deleteAll();
        Assert.assertEquals(0, tasksRestEndpoint.count());
    }

}
