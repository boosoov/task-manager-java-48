package com.rencredit.jschool.boruak.taskmanager.restendpoint;

import com.rencredit.jschool.boruak.taskmanager.config.ClientConfiguration;
import com.rencredit.jschool.boruak.taskmanager.dto.ProjectDTO;
import com.rencredit.jschool.boruak.taskmanager.endpoint.server.IAuthRestEndpoint;
import com.rencredit.jschool.boruak.taskmanager.endpoint.server.IProjectRestEndpoint;
import com.rencredit.jschool.boruak.taskmanager.marker.IntegrationWithServerTestCategory;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ClientConfiguration.class)
@Category(IntegrationWithServerTestCategory.class)
public class IProjectRestEndpointTest {

    @Autowired
    IProjectRestEndpoint projectRestEndpoint;

    @Autowired
    IAuthRestEndpoint authRestEndpoint;

    @Before
    public void init()  {
        authRestEndpoint.login("test", "test");
    }

    @After
    public void clearAll() {
        authRestEndpoint.logout();
    }

    @Test
    public void test() {
        final ProjectDTO project = new ProjectDTO();
        project.setDescription("111111");
        projectRestEndpoint.create(project);

        final ProjectDTO projectFromWeb = projectRestEndpoint.findOneByIdDTO(project.getId());
        Assert.assertEquals(project.getId(), projectFromWeb.getId());

        Assert.assertTrue(projectRestEndpoint.existsById(project.getId()));

        projectRestEndpoint.deleteOneById(project.getId());
        Assert.assertFalse(projectRestEndpoint.existsById(project.getId()));
    }

}
