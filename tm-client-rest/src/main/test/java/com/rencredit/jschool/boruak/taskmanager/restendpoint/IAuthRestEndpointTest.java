package com.rencredit.jschool.boruak.taskmanager.restendpoint;

import com.rencredit.jschool.boruak.taskmanager.config.ClientConfiguration;
import com.rencredit.jschool.boruak.taskmanager.dto.UserDTO;
import com.rencredit.jschool.boruak.taskmanager.dto.status.Result;
import com.rencredit.jschool.boruak.taskmanager.endpoint.server.IAuthRestEndpoint;
import com.rencredit.jschool.boruak.taskmanager.endpoint.server.IProjectRestEndpoint;
import com.rencredit.jschool.boruak.taskmanager.marker.IntegrationWithServerTestCategory;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ClientConfiguration.class)
@Category(IntegrationWithServerTestCategory.class)
public class IAuthRestEndpointTest {

    @Autowired
    IProjectRestEndpoint projectRestEndpoint;

    @Autowired
    IAuthRestEndpoint authRestEndpoint;

    @Before
    public void init()  {

    }

    @After
    public void clearAll() {

    }

    @Test
    public void test() {

        Result result = authRestEndpoint.login("test", "test");
        System.out.println(result.success);
        Assert.assertTrue(result.success);
        UserDTO user = authRestEndpoint.profile();
        Assert.assertEquals("test", user.getLogin());

        result = authRestEndpoint.logout();
        Assert.assertTrue(result.success);
        UserDTO userEmpty;
        try {
            userEmpty = authRestEndpoint.profile();
        } catch (Exception e) {
            userEmpty = null;
        }
        Assert.assertNull(userEmpty);

    }

}
