package com.rencredit.jschool.boruak.taskmanager.repository.entity;

import com.rencredit.jschool.boruak.taskmanager.config.WebApplicationConfiguration;
import com.rencredit.jschool.boruak.taskmanager.entity.Project;
import com.rencredit.jschool.boruak.taskmanager.entity.Project;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.repository.entity.IProjectRepositoryEntity;
import com.rencredit.jschool.boruak.taskmanager.repository.entity.IProjectRepositoryEntity;
import com.rencredit.jschool.boruak.taskmanager.repository.entity.IUserRepositoryEntity;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.*;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WebApplicationConfiguration.class)
public class ProjectRepositoryEntityTest {

    @Autowired
    private IProjectRepositoryEntity projectRepository;

    @Autowired
    private IUserRepositoryEntity userRepository;

    private User user;
    
    @Before
    public void setup() {
        projectRepository.deleteAll();
        userRepository.deleteAll();
        user = new User("login", "password");
        userRepository.save(user);
    }

    @Test
    public void testSave() {
        final Project project = new Project(user, "name");
        projectRepository.save(project);
        assertNotNull(projectRepository.findById(project.getId()));
    }

    @Test
    public void testFindById() {
        final Project project = new Project(user, "name");
        projectRepository.save(project);
        final Project projectFromBase = projectRepository.findById(project.getId()).orElse(null);
        assertNotNull(projectFromBase);
        assertEquals(project.getId(), projectFromBase.getId());
    }

    @Test
    public void testExistById() {
        final Project project = new Project(user, "name");
        projectRepository.save(project);
        assertTrue(projectRepository.existsById(project.getId()));
        assertFalse(projectRepository.existsById("34234"));
    }

    @Test
    public void testExistByUserIdAndId() {
        final Project project = new Project(user, "name");
        projectRepository.save(project);
        assertTrue(projectRepository.existsByUserIdAndId(user.getId(), project.getId()));
        assertFalse(projectRepository.existsByUserIdAndId(user.getId(), "34234"));
    }

    @Test
    public void testUpdate() {
        Project project = new Project(user, "name");
        projectRepository.save(project);
        final Project projectFromBase = projectRepository.findById(project.getId()).orElse(null);
        assertNotNull(projectFromBase);
        assertEquals(project.getName(), projectFromBase.getName());

        project.setName("name2");
        projectRepository.save(project);
        final Project projectFromBaseWithAnotherLogin = projectRepository.findById(project.getId()).orElse(null);
        assertNotNull(projectFromBaseWithAnotherLogin);
        assertEquals(project.getId(), projectFromBaseWithAnotherLogin.getId());
        assertEquals(project.getName(), projectFromBaseWithAnotherLogin.getName());
    }

    @Test
    public void testFindAll() {
        projectRepository.save(new Project());
        projectRepository.save(new Project());
        projectRepository.save(new Project());
        assertEquals(3, projectRepository.findAll().size());
    }

    @Test
    public void testFindAllByUserId() {
        projectRepository.save(new Project(user, "name"));
        projectRepository.save(new Project(user, "name"));
        projectRepository.save(new Project());
        assertEquals(2, projectRepository.findAllByUserId(user.getId()).size());
    }

    @Test
    @Transactional
    public void testDeleteByUserIdAndId() {
        final Project project = new Project(user, "name");
        projectRepository.save(project);
        final Project projectFromBase = projectRepository.findById(project.getId()).orElse(null);
        assertNotNull(projectFromBase);
        projectRepository.deleteByUserIdAndId(user.getId(), project.getId());
        final Project projectFromBaseAfterDelete = projectRepository.findById(project.getId()).orElse(null);
        assertNull(projectFromBaseAfterDelete);
    }

    @Test
    @Transactional
    public void testDeleteByUserIdAndName() {
        final Project project = new Project(user, "name");
        projectRepository.save(project);
        final Project projectFromBase = projectRepository.findById(project.getId()).orElse(null);
        assertNotNull(projectFromBase);
        projectRepository.deleteByUserIdAndName(user.getId(), project.getName());
        final Project projectFromBaseAfterDelete = projectRepository.findById(project.getId()).orElse(null);
        assertNull(projectFromBaseAfterDelete);
    }

    @Test
    @Transactional
    public void testDeleteAllByUserId() {
        projectRepository.save(new Project());
        projectRepository.save(new Project());
        projectRepository.save(new Project());
        assertEquals(3, projectRepository.findAll().size());
        projectRepository.deleteAll();
        assertEquals(0, projectRepository.findAll().size());
    }

}
