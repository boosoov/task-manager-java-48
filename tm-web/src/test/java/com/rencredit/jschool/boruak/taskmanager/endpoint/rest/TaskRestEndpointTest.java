package com.rencredit.jschool.boruak.taskmanager.endpoint.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.rencredit.jschool.boruak.taskmanager.config.WebApplicationConfiguration;
import com.rencredit.jschool.boruak.taskmanager.dto.TaskDTO;
import com.rencredit.jschool.boruak.taskmanager.entity.Task;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.repository.entity.ITaskRepositoryEntity;
import com.rencredit.jschool.boruak.taskmanager.service.TaskService;
import com.rencredit.jschool.boruak.taskmanager.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WebApplicationConfiguration.class)
public class TaskRestEndpointTest {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private UserService userService;

    @Autowired
    private TaskService taskService;

    private MockMvc mockMvc;

    private User admin;

    @Before
    public void setup() throws EmptyLoginException, EmptyUserException, BusyLoginException, DeniedAccessException, EmptyPasswordException, EmptyRoleException, EmptyHashLineException {
        this.mockMvc = MockMvcBuilders
                        .webAppContextSetup(this.webApplicationContext)
                        .build();

        SecurityContext ctx = SecurityContextHolder.createEmptyContext();
        SecurityContextHolder.setContext(ctx);
        ctx.setAuthentication(new UsernamePasswordAuthenticationToken("admin", "", Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"))));
        userService.deleteAll();
        taskService.deleteAll();
        admin = userService.findByLoginEntity("admin");

        final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("admin", "admin");
        final Authentication authentication =
                authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @Test
    @Transactional
    public void testCreate() throws Exception {
        final Task task = new Task();
        task.setName("task");
        task.setDescription("task");
        task.setUser(admin);
        final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        final ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(task);

        ResultActions result = mockMvc.perform(MockMvcRequestBuilders
                                .post("/api/task")
                                .contentType(MediaType.APPLICATION_JSON_UTF8)
                                .content(requestJson));

//        result.andDo(print());
        result.andExpect(MockMvcResultMatchers.status().isOk());
        final TaskDTO taskFromBase = taskService.findOneByIdDTO(admin.getId(), task.getId());
        assertEquals(task.getName(), taskFromBase.getName());
    }

    @Test
    public void testFindOneByIdDTO() throws Exception {
        final TaskDTO task = new TaskDTO();
        task.setName("taskName");
        task.setDescription("taskDescription");
        task.setUserId(admin.getId());
        taskService.save(task);

        ResultActions result = mockMvc.perform(MockMvcRequestBuilders
                .get("/api/task/{id}", task.getId())
                .accept(MediaType.APPLICATION_JSON));

        result.andDo(print());
        result.andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.name").value(task.getName()))
                .andExpect(jsonPath("$.description").value(task.getDescription()));

    }

    @Test
    public void testExistsById() throws Exception {
        final TaskDTO task = new TaskDTO();
        task.setName("taskName");
        task.setDescription("taskDescription");
        task.setUserId(admin.getId());
        taskService.save(task);

        ResultActions result = mockMvc.perform(MockMvcRequestBuilders
                .get("/api/task/exist/{id}", task.getId())
                .accept(MediaType.APPLICATION_JSON));

//        result.andDo(print());
        result.andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string("true"));

        result = mockMvc.perform(MockMvcRequestBuilders
                .get("/api/task/exist/{id}", "dfsdfsd")
                .accept(MediaType.APPLICATION_JSON));

//        result.andDo(print());
        result.andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string("false"));
    }

    @Test
    public void testDeleteOneById() throws Exception {
        final TaskDTO task = new TaskDTO();
        task.setName("taskName");
        task.setDescription("taskDescription");
        task.setUserId(admin.getId());
        taskService.save(task);
        assertNotNull(taskService.findOneByIdDTO(admin.getId(), task.getId()));

        ResultActions result = mockMvc.perform(MockMvcRequestBuilders
                .delete("/api/task/{id}", task.getId())
                .accept(MediaType.APPLICATION_JSON));

        result.andExpect(MockMvcResultMatchers.status().isOk());
        assertNull(taskService.findOneByIdDTO(admin.getId(), task.getId()));

    }

}
