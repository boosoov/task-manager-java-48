<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>--%>
<jsp:include page="../../include/_header.jsp"/>
<h2>TASK LIST</h2>

<table width="100%" cellpadding="10" border="1" style="border-collapse: collapse;">
    <tr>
        <th width="200" nowrap="nowrap">ID</th>
        <th width="200" nowrap="nowrap">PROJECT</th>
        <th width="200" nowrap="nowrap">NAME</th>
        <th width="100%" align="left">DESCRIPTION</th>
        <th width="200" align="left">STATUS</th>
        <th width="200" align="left">DATE START</th>
        <th width="200" align="left">DATE FINISH</th>
        <th width="100" nowrap="nowrap" align="center">EDIT</th>
        <th width="100" nowrap="nowrap" align="center">DELETE</th>
    </tr>
    <c:forEach var="task" items="${tasks}">
        <tr>
            <td>
                <c:out value="${task.id}" />
            </td>
            <td>
                <c:out value="${task.project.name}" />
            </td>
            <td>
                <c:out value="${task.name}" />
            </td>
            <td>
                <c:out value="${task.description}" />
            </td>
            <td>
                <c:out value="${task.status.displayName}" />
            </td>
            <td>
                <fmt:formatDate type="date" value="${task.dateStart}" />
            </td>
            <td>
                <fmt:formatDate type="date" value="${task.dateFinish}" />
            </td>
            <td align="center">
                <a href="/task/edit/${task.id}">EDIT</a>
            </td>
            <td align="center">
                <a href="/task/delete/${task.id}">DELETE</a>
            </td>
        </tr>
    </c:forEach>
</table>

<form action="/task/create" style="margin-top: 20px;">
    <button>CREATE TASK</button>
</form>

<jsp:include page="../../include/_footer.jsp"/>

