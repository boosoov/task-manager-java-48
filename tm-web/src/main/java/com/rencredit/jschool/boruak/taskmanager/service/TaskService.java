package com.rencredit.jschool.boruak.taskmanager.service;

import com.rencredit.jschool.boruak.taskmanager.api.service.ITaskService;
import com.rencredit.jschool.boruak.taskmanager.dto.TaskDTO;
import com.rencredit.jschool.boruak.taskmanager.entity.Task;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectIndexException;
import com.rencredit.jschool.boruak.taskmanager.repository.dto.ITaskRepositoryDTO;
import com.rencredit.jschool.boruak.taskmanager.repository.entity.ITaskRepositoryEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class TaskService implements ITaskService {

    @Autowired
    private ITaskRepositoryDTO repositoryDTO;

    @Autowired
    private ITaskRepositoryEntity repositoryEntity;

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public void save(@Nullable final String userId, @Nullable final String name) throws EmptyNameException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final TaskDTO task = new TaskDTO(userId, name);
        repositoryDTO.save(task);
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public void save(@Nullable final String userId, @Nullable final String name, @Nullable final String description) throws EmptyDescriptionException, EmptyNameException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final TaskDTO task = new TaskDTO(userId, name, description);
        repositoryDTO.save(task);
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public void save(@Nullable final TaskDTO task) throws EmptyTaskException, EmptyUserIdException {
        if (task == null) throw new EmptyTaskException();
        repositoryDTO.save(task);
    }

    @Transactional
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public List<TaskDTO> saveList(@Nullable final Iterable<TaskDTO> iterable) throws EmptyTaskListException {
        if (iterable == null) throw new EmptyTaskListException();
        return repositoryDTO.saveAll(iterable);
    }



    @NotNull
    @Override
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public List<TaskDTO> findAllByUserIdDTO(@Nullable final String userId) throws EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final List<TaskDTO> tasks = repositoryDTO.findAllByUserId(userId);
        return tasks;
    }

    @NotNull
    @Override
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public List<Task> findAllByUserIdEntity(@Nullable final String userId) throws EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return repositoryEntity.findAllByUserId(userId);
    }

    @NotNull
    @Override
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public List<TaskDTO> findAllByProjectIdDTO(@Nullable final String projectId) throws EmptyProjectIdException {
        if (projectId == null || projectId.isEmpty()) throw new EmptyProjectIdException();
        @NotNull final List<TaskDTO> tasks = repositoryDTO.findAllByProject(projectId);
        return tasks;
    }

    @Nullable
    @Override
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public TaskDTO findOneByIdDTO(@Nullable final String userId, @Nullable final String id) throws EmptyIdException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final Optional<TaskDTO> opt = repositoryDTO.findById(id);
        return opt.isPresent() ? opt.get() : null;
    }

    @Nullable
    @Override
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public TaskDTO findOneByIndexDTO(@Nullable final String userId, @Nullable final Integer index) throws EmptyUserIdException, IncorrectIndexException {
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final TaskDTO task = findAllByUserIdDTO(userId).get(index);
        return task;
    }

    @Nullable
    @Override
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public TaskDTO findOneByNameDTO(@Nullable final String userId, @Nullable final String name) throws EmptyUserIdException, EmptyNameException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @Nullable final TaskDTO task = repositoryDTO.findByUserIdAndName(userId, name);
        return task;
    }

    @NotNull
    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<TaskDTO> findListDTO() {
        @NotNull final List<TaskDTO> tasks = repositoryDTO.findAll();
        return tasks;
    }

    @NotNull
    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<Task> findListEntity() {
        @NotNull final List<Task> tasks = repositoryEntity.findAll();
        return tasks;
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public void attachTaskToProject(
            @Nullable final String userId,
            @Nullable final String taskId,
            @Nullable final String projectId
    ) throws EmptyTaskException, EmptyIdException, EmptyUserIdException, EmptyTaskIdException, EmptyProjectIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyTaskIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyProjectIdException();
        @Nullable final TaskDTO task = findOneByIdDTO(userId, taskId);
        if (task == null) throw new EmptyTaskException();
        task.setProject(projectId);
        repositoryDTO.save(task);
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public void detachTaskToProject(
            @Nullable final String userId,
            @Nullable final String taskId,
            @Nullable final String projectId
    ) throws EmptyTaskException, EmptyIdException, EmptyUserIdException, EmptyTaskIdException, EmptyProjectIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyTaskIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyProjectIdException();

        @Nullable final TaskDTO task = findOneByIdDTO(userId, taskId);
        if (task == null) throw new EmptyTaskException();
        task.setProject(null);
        repositoryDTO.save(task);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public boolean existsById(@Nullable final String taskId) throws EmptyTaskIdException {
        if (taskId == null || taskId.isEmpty()) throw new EmptyTaskIdException();
        return repositoryDTO.existsById(taskId);
    }

    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public boolean existsByUserIdAndTaskId(@Nullable final String userId, @Nullable final String taskId) throws EmptyUserIdException, EmptyTaskIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyTaskIdException();
        return repositoryEntity.existsByUserIdAndId(userId, taskId);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public long count() {
        return repositoryDTO.count();
    }

    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public long countAllByUserId(@NotNull final String userId) {
        return repositoryDTO.countAllByUserId(userId);
    }



    @Override
    @Nullable
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public Integer deleteById(@Nullable final String userId, @Nullable final String id) throws EmptyUserIdException, EmptyIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repositoryEntity.deleteByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public void deleteByIndex(@Nullable final String userId, @Nullable final Integer index) throws IncorrectIndexException, EmptyUserIdException, EmptyTaskException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();

        @Nullable final TaskDTO task = findOneByIndexDTO(userId, index);
        if (task == null) throw new EmptyTaskException();

        @NotNull final String taskId = task.getId();
        repositoryEntity.deleteById(taskId);
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public void deleteByName(@Nullable final String userId, @Nullable final String name) throws EmptyNameException, EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        repositoryEntity.deleteByUserIdAndName(userId, name);
    }

    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void deleteList(@Nullable final Iterable<Task> iterable) throws EmptyTaskListException {
        if (iterable == null) throw new EmptyTaskListException();

        repositoryEntity.deleteAll(iterable);
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public void deleteAllByUserId(@Nullable final String userId) throws EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();

        repositoryEntity.deleteAllByUserId(userId);
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void deleteAll() {
        repositoryEntity.deleteAll();
    }

}
