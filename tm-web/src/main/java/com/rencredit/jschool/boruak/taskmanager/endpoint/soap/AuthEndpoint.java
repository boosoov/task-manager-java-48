package com.rencredit.jschool.boruak.taskmanager.endpoint.soap;

import com.rencredit.jschool.boruak.taskmanager.api.endpoint.soap.IAuthEndpoint;
import com.rencredit.jschool.boruak.taskmanager.api.service.IAuthService;
import com.rencredit.jschool.boruak.taskmanager.dto.UserDTO;
import com.rencredit.jschool.boruak.taskmanager.dto.status.Result;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.unknown.UnknownUserException;
import com.rencredit.jschool.boruak.taskmanager.service.UserService;
import com.rencredit.jschool.boruak.taskmanager.util.UserUtil;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * http://localhost:8080/services/AuthEndpoint?wsdl
 */

@Component
@WebService
public class AuthEndpoint implements IAuthEndpoint {

    @Autowired
    private UserService userService;

    @Nullable
    @Autowired
    private IAuthService authService;

    @Resource
    private AuthenticationManager authenticationManager;

    @Override
    @WebMethod
    public Result login(
            @WebParam(name = "username") final String username,
            @WebParam(name = "password") final String password
    ) throws EmptyLoginException, UnknownUserException, DeniedAccessException {
        UserDTO user = userService.findByLoginDTO(username);
        if (user == null) throw new UnknownUserException();
        if (user.isLocked()) throw new DeniedAccessException();
        final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
        final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        return new Result(authentication.isAuthenticated());
    }

    @Override
    @WebMethod
    public Result logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return new Result();
    }

    @Override
    @WebMethod
    public UserDTO profile() throws DeniedAccessException, UnknownUserException, EmptyIdException {
        return userService.findByIdDTO(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    public void registrationLoginPassword(
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password
    ) throws EmptyHashLineException, BusyLoginException, EmptyPasswordException, EmptyLoginException, EmptyUserException, DeniedAccessException {
        authService.registration(login, password);
    }

}
