package com.rencredit.jschool.boruak.taskmanager.dto;

import java.util.Date;

public class DateDTO {

    private long date;

    public DateDTO(Date date) {
        this.date = date.getTime();
    }
}
