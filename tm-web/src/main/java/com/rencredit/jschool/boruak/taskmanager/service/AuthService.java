package com.rencredit.jschool.boruak.taskmanager.service;

import com.rencredit.jschool.boruak.taskmanager.api.service.IAuthService;
import com.rencredit.jschool.boruak.taskmanager.api.service.IUserService;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.notexist.NotExistUserException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class AuthService implements IAuthService {

    @NotNull
    @Autowired
    private IUserService userService;

    @Override
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public boolean checkRoles(@Nullable final String userId, @Nullable final Role[] roles) throws DeniedAccessException, EmptyUserIdException, EmptyRoleException, NotExistUserException, EmptyIdException {
        if (roles == null || roles.length == 0) throw new EmptyRoleException();
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();

        @Nullable final User user = userService.findByIdEntity(userId);
        if (user == null) throw new NotExistUserException();
        @Nullable final Role role = user.getRole();
        for (final Role item : roles) if (role.equals(item)) return true;
        throw new DeniedAccessException();
    }

    @Override
    public void registration(@Nullable final String login, @Nullable final String password) throws EmptyPasswordException, EmptyLoginException, EmptyUserException, BusyLoginException, DeniedAccessException, EmptyHashLineException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();

        userService.save(login, password);
    }

    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void registration(@Nullable final String login, @Nullable final String password, @Nullable final Role role) throws EmptyRoleException, EmptyPasswordException, EmptyLoginException, EmptyUserException, BusyLoginException, DeniedAccessException, EmptyHashLineException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();

        userService.save(login, password, role);
    }

}
