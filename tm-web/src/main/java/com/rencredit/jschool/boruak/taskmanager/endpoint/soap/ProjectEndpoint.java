package com.rencredit.jschool.boruak.taskmanager.endpoint.soap;

import com.rencredit.jschool.boruak.taskmanager.dto.ProjectDTO;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.unknown.UnknownUserException;
import com.rencredit.jschool.boruak.taskmanager.service.ProjectService;
import com.rencredit.jschool.boruak.taskmanager.service.UserService;
import com.rencredit.jschool.boruak.taskmanager.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

/**
 * http://localhost:8080/services/ProjectEndpoint?wsdl
 */

@Component
@WebService
public class ProjectEndpoint {

    @Autowired
    private ProjectService projectService;

    @Autowired
    private UserService userService;

    @WebMethod
    public void createProject(
            @WebParam(name = "project") ProjectDTO project
    ) throws EmptyProjectException, EmptyUserIdException, EmptyLoginException, DeniedAccessException, UnknownUserException {
        projectService.save(UserUtil.getUserId(), project);
    }

    @WebMethod
    public ProjectDTO findProjectByIdDTO(
            @WebParam(name = "id") String id
    ) throws EmptyIdException, EmptyUserIdException, DeniedAccessException, UnknownUserException {
        return projectService.findByIdDTO(UserUtil.getUserId(), id);
    }

    @WebMethod
    public void deleteProject(
            @WebParam(name = "project") ProjectDTO project
    ) throws EmptyProjectException, EmptyUserIdException, DeniedAccessException, UnknownUserException {
        projectService.delete(UserUtil.getUserId(), project);
    }

    @WebMethod
    public void deleteProjectById(
            @WebParam(name = "id") String id
    ) throws DeniedAccessException, UnknownUserException, EmptyUserIdException, EmptyProjectIdException {
        projectService.deleteByUserIdAndProjectId(UserUtil.getUserId(), id);
    }

    @WebMethod
    public List<ProjectDTO> getListProjects() throws EmptyUserIdException, DeniedAccessException, UnknownUserException {
        return projectService.findAllByUserIdDTO(UserUtil.getUserId());
    }

    @WebMethod
    public List<ProjectDTO> getListAllUsersProjects() {
        return projectService.findList();
    }

    @WebMethod
    public void deleteAllProjects() throws EmptyUserIdException, DeniedAccessException, UnknownUserException {
        projectService.deleteAllByUserId(UserUtil.getUserId());
    }

    @WebMethod
    public boolean existsProjectById(
            @WebParam(name = "id") String id
    ) throws DeniedAccessException, UnknownUserException, EmptyUserIdException, EmptyProjectIdException {
        return projectService.existsByUserIdAndProjectId(UserUtil.getUserId(), id);
    }

}
