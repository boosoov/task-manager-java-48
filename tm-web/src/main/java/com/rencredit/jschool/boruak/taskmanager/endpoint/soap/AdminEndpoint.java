package com.rencredit.jschool.boruak.taskmanager.endpoint.soap;

import com.rencredit.jschool.boruak.taskmanager.api.endpoint.soap.IAdminEndpoint;
import com.rencredit.jschool.boruak.taskmanager.service.PropertyService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 * http://localhost:8080/services/AdminEndpoint?wsdl
 */

@Component
@WebService
public class AdminEndpoint implements IAdminEndpoint {

    @NotNull
    @Autowired
    private PropertyService propertyService;

    public AdminEndpoint() {
    }

    @Override
    @WebMethod
    public String getHostPort()  {
        return "HOST:" + propertyService.getServerHost() + "\n" + "PORT:" + propertyService.getServerPort();
    }

    @Override
    @WebMethod
    public String getHost() {
        System.err.println(propertyService.getServerHost());
        return propertyService.getServerHost();
    }

    @Override
    @WebMethod
    public Integer getPort() {
        return propertyService.getServerPort();
    }

    @Override
    @WebMethod
    public void shutDownServer() {
        System.exit(0);
    }

}

