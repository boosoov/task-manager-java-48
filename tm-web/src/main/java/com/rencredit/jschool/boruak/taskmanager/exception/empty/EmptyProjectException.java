package com.rencredit.jschool.boruak.taskmanager.exception.empty;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractException;

public class EmptyProjectException extends AbstractException {

    public EmptyProjectException() {
        super("Error! Project not exist...");
    }

}
