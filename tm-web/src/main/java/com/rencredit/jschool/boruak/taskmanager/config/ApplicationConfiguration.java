package com.rencredit.jschool.boruak.taskmanager.config;

import com.rencredit.jschool.boruak.taskmanager.endpoint.soap.*;
import org.apache.cxf.Bus;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.WebApplicationInitializer;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import javax.xml.ws.Endpoint;


@Configuration
public class ApplicationConfiguration {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public Endpoint projectEndpointRegistry(final ProjectEndpoint projectEndpoint, Bus bus) {
        final EndpointImpl endpoint = new EndpointImpl(bus, projectEndpoint);
        endpoint.publish("/ProjectEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint taskEndpointRegistry(final TaskEndpoint taskEndpoint, Bus bus) {
        final EndpointImpl endpoint = new EndpointImpl(bus, taskEndpoint);
        endpoint.publish("/TaskEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint userEndpointRegistry(final UserEndpoint userEndpoint, Bus bus) {
        final EndpointImpl endpoint = new EndpointImpl(bus, userEndpoint);
        endpoint.publish("/UserEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint authEndpointRegistry(final AuthEndpoint authEndpoint, Bus bus) {
        final EndpointImpl endpoint = new EndpointImpl(bus, authEndpoint);
        endpoint.publish("/AuthEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint adminEndpointRegistry(final AdminEndpoint adminEndpoint, Bus bus) {
        final EndpointImpl endpoint = new EndpointImpl(bus, adminEndpoint);
        endpoint.publish("/AdminEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint adminUserEndpointRegistry(final AdminUserEndpoint adminUserEndpoint, Bus bus) {
        final EndpointImpl endpoint = new EndpointImpl(bus, adminUserEndpoint);
        endpoint.publish("/AdminUserEndpoint");
        return endpoint;
    }

}
